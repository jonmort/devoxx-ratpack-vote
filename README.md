# REST API

FORMAT: 1A
HOST: https://api-voting.devoxx.com

# Devoxx Vote API
The Devoxx Vote API is a service that allows Devoxxians to vote on the talks.
A vote can be cast a talk has started.
Votes are:

* Tied to user id which comes from Eventbrite and is an integer
* From 1-5:
 * 5: Awesome
 * 4: Good
 * 3: OK
 * 2: Meh
 * 1: Terrible


# Vote [/:conferenceId/vote]

## Submit a vote [POST]

Vote on a talk. Vote is from 1-5 and is made on a talk.

A vote has the following attributes:

+ user (number) - The EventBrite user id
+ rating (number)- A rating from 1-5
+ talkId (string) - A talk to vote on. This id is defined by http://cfp.devoxx.be/api/profile/talk

+ Request (application/json)

            {
                "rating": 5,
                "talkId": "MBG-3462",
                "user": 10
            }


+ Response 201 (application/json)

    + Body

            {
                "rating": 5,
                "talkId": "MBG-3462",
                "user": 10
            }

+ Response 500 (application/json)

    + Body

            {
                "message": "Cannot vote on talk yet"
            }


+ Response 409 (application/json)

    + Body

            {
                "message": "Cannot vote more than once"
            }


## AlternativeSubmit a vote [POST]

Vote on a talk. Vote is from 1-5 and is made on a talk.

A vote has the following attributes:

+ user (string) - The user
+ details (array) - array of details containing
     + rating (number) - the rating
     + aspect (string) - named aspect to vote on ("style" for example)
     + review (string) - detailed review
+ talkId (string) - A talk to vote on. This id is defined by http://cfp.devoxx.be/api/profile/talk

+ Request (application/json)

            {
                "details": [{
                    "rating": 5,
                    "aspect": "detail",
                    "review": "This talk had really good detail"
                },{
                    "rating": 2,
                    "aspect": "slides",
                    "review": "Slides were just bullet points"
                },{
                    "rating": 5,
                    "aspect": "demo",
                    "review": "All the demos worked first time"
                }],
                "talkId": "MBG-3462",
                "user": 10
            }


+ Response 201 (application/json)

    + Body

            {
                "rating": 5,
                "talkId": "MBG-3462",
                "user": 10
            }

+ Response 500 (application/json)

    + Body

            {
                "message": "Cannot vote on talk yet"
            }


+ Response 409 (application/json)

    + Body

            {
                "message": "Cannot vote more than once"
            }



# Top Talks [/:conferenceId/top/talks{?limit}&{day}&{talkType}&{track}]

## Talks [GET]

Retrieve the top talks as voted on. Talks are sorted by average rating.
Each talk has the following attributes:

+ avg (number) - The average rating for the talk (sum/count)
+ count (number) - The total number of votes cast
+ sum (number) - The cumlative rating
+ name (string) - The talk ID

+ Parameters

    + limit (number, optional) - The maximum number of results to return. 1-100
        + Default: `10`
    + day (string, optional) - The day to filter by (monday, tuesday, wednesday, thursday, friday)
        + Default: empty
    + talkType (string, optiona) - Talk type to filter by
        + Default: empty
    + track (string, optional) - Track to filter by
        + Default: empty

+ Response 200 (application/json)

    + Body

            {
               "talks" : [
                  {
                     "avg" : "5",
                     "count" : "2",
                     "sum" : "10",
                     "name" : "VML-9224"
                  },
                  {
                     "count" : "85",
                     "avg" : "3.0588235294117645",
                     "sum" : "260",
                     "name" : "AOG-3279"
                  },
                  {
                     "count" : "88",
                     "avg" : "3.0454545454545454",
                     "name" : "YIW-0702",
                     "sum" : "268"
                  },
                  {
                     "name" : "CKD-6073",
                     "sum" : "240",
                     "avg" : "3",
                     "count" : "80"
                  },
                  {
                     "name" : "MBG-3462",
                     "sum" : "288",
                     "count" : "100",
                     "avg" : "2.8799999999999999"
                  },
                  {
                     "count" : "100",
                     "avg" : "2.77",
                     "name" : "DIU-5372",
                     "sum" : "277"
                  },
                  {
                     "count" : "4",
                     "avg" : "2",
                     "sum" : "8",
                     "name" : "DNL-9930"
                  }
               ]
            }

# Talk [/:conferenceId/talk/:talkId]

## Categories [GET]

Retrieve the details of a specific talk


+ Response 200 (application/json)

    + Body

            {
               "sum" : "5",
               "count" : "1",
               "title" : "Apache Sling as an OSGi-powered REST middleware",
               "summary" : "<p>Apache Sling is an innovative web ....",
               "avg" : "5.000000",
               "name" : "RRH-9866",
               "type" : "Tools-in-Action",
               "track" : "Server Side Java",
               "speakers" : [
                  "Robert Munteanu"
               ]
            }


# Categories [/:conferenceId/categories]

## Categories [GET]

Retrieve the categories that can be filtered by. day, track and talkType can all
be passed to top/talks fo filter the top talks. Each of the categories can be used
as a filter value.


+ Response 200 (application/json)

    + Body

            {
               "tracks" : [
                  "Server Side Java",
                  "Startups",
                  "Java SE",
                  "Architecture & Security",
                  "Methodology & DevOps",
                  "JVM Languages",
                  "Future<Devoxx>",
                  "Mobile",
                  "Cloud & BigData",
                  "Web & HTML5"
               ],
               "days" : [
                  "friday",
                  "thursday",
                  "tuesday",
                  "monday",
                  "wednesday"
               ],
               "talkTypes" : [
                  "Tools-in-Action",
                  "Hand's on Labs",
                  "BOF (Bird of a Feather)",
                  "Quickie",
                  "Ignite Sessions",
                  "Conference",
                  "University",
                  "Startup presentation"
               ]
            }
