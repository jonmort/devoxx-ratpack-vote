package cfp

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class CfpSlurpTask extends DefaultTask {

    def slurper = new JsonSlurper()

    def apiRoot = "http://cfp.devoxx.be/"

    def File outputDir

    @TaskAction
    def slurp() {

        def conferences = slurper.parse(new URL("${apiRoot}/api/conferences")).links*.href.collect {
            it.tokenize("/").last()
        }
        println "Found conferences ${conferences.join(',')} to slurp"
        conferences.each { slurpConference((String)it) }
    }

    def slurpConference(String conference) {
        println "Slurping ${conference}...  "
        def schedules = slurper.parse(new URL("${apiRoot}/api/conferences/${conference}/schedules/"))
        def talks = schedules.links*.href.collect { String href ->
            slurper.parse(new URL(href)).slots.findAll { it.talk }
        }.flatten()

        def speakers = slurper.parse(new URL("${apiRoot}/api/conferences/${conference}/speakers"))
        def byDay = talks.groupBy { it.day }

        byDay.each { String d, List<Object> t ->
            def dayDir = new File(new File(new File(outputDir, conference), 'days'), d)
            if (!dayDir.exists()) {
                dayDir.mkdirs()
            }
            t.each { talk ->
                def out = new File(dayDir, "${talk.talk.id}.json")
                out.write(JsonOutput.toJson(talk), "UTF-8")
            }
        }

        def speakersDir = new File(new File(outputDir, conference), 'speakers')
        if (!speakersDir.exists()) {
            speakersDir.mkdirs()
        }
        speakers.each { speaker ->
            def out = new File(speakersDir, "${speaker.uuid}.json")
            out.write(JsonOutput.toJson(speaker), "UTF-8")
        }
    }
}
