package cfp

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

import static org.junit.Assert.assertTrue

class CfpSlurpTaskTest {

    @Test
    public void canAddTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        def task = project.task('slurp', type: CfpSlurpTask)
        assertTrue(task instanceof CfpSlurpTask)
    }
}
