package devoxx.vote

import com.fasterxml.jackson.databind.ObjectMapper
import model.Conference
import model.Slot
import model.Speaker
import model.Talk
import org.reflections.Reflections
import org.reflections.scanners.ResourcesScanner

import java.util.regex.Pattern

class TalkService {

    final Map<String, Conference> conferences = readSlots()
    final Map<String, Slot> slots = makeSlots(conferences.values())

    Map<String, Slot> makeSlots(Collection<Conference> confs) {
        confs*.slots.collectEntries { slot ->
            [slot.talk.id, slot]
        }
    }

    private static Map<String, Conference> readSlots() {
        ObjectMapper mapper = new ObjectMapper()
        Reflections reflections = new Reflections('cfp', new ResourcesScanner())
        Set<List<String>> resourceList = reflections.getResources(Pattern.compile(/.*\.json/))*.tokenize('/')
        def slotPaths = resourceList.findAll { it[2] == 'days' }
        def conferenceToSlotPath = slotPaths.groupBy { it[1] }
        def speakerPaths = resourceList.findAll { it[2] == 'speakers' }
        def conferenceToSpeakerPath = speakerPaths.groupBy { it[1] }
        def conferences = resourceList.collect { it[1] }.unique()

        conferences.collectEntries { conference ->
            def slots = conferenceToSlotPath[conference].collect { path ->
                mapper.readValue(TalkService.classLoader.getResourceAsStream(path.join(File.separator)), Slot)
            }
            def speakers = conferenceToSpeakerPath[conference].collect { path ->
                mapper.readValue(TalkService.classLoader.getResourceAsStream(path.join(File.separator)), Speaker)
            }
            [conference, new Conference(slots, speakers, conference)]
        }
    };

    boolean talkOpen(String talkId) {
        if (System.getenv('TESTING') == 'true') {
            return true
        }
        Slot slot = slots.values().flatten().find { Slot slot ->
            slot.talk.id == talkId
        }
        slot.fromTimeMillis <= System.currentTimeMillis()
    }

    boolean talkExists(String talkId) {
        slots.values()*.talk.id.flatten().contains(talkId)
    }

    Collection<?> talkIdsForConference(String conference) {
        conferences[conference]?.slots*.talk*.id ?: []
    }

    Talk getTalkById(String talkId) {
        slots.values()*.talk.flatten().find { Talk t -> t.id == talkId }
    }

    Slot getSlotByTalkId(String talkId) {
        slots.values().flatten().find { it.talk.id == talkId }
    }
}
