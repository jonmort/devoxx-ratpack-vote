package devoxx.vote

import com.google.inject.Provides
import com.google.inject.Scopes
import groovy.transform.Canonical
import org.ektorp.CouchDbConnector
import org.ektorp.CouchDbInstance
import org.ektorp.http.HttpClient
import org.ektorp.http.StdHttpClient
import org.ektorp.impl.StdCouchDbInstance
import ratpack.guice.ConfigurableModule

class VoteModule extends ConfigurableModule<DbConfig> {
    @Override
    protected void configure() {
        bind(TalkService).in(Scopes.SINGLETON)
        bind(VoteRepository).in(Scopes.SINGLETON)
    }

    @Provides
    static CouchDbConnector provideCouchDbConnector(DbConfig config) {
        HttpClient httpClient = new StdHttpClient.Builder()
                .url(config.url)
                .username(config.username)
                .password(config.password)
                .build();
        CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient);
        dbInstance.createConnector(config.name, true);
    }

    @Canonical
    static class DbConfig {
        String name, url, username, password
    }
}
