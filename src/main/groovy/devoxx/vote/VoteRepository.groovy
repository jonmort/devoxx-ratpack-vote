package devoxx.vote

import com.google.inject.Inject
import devoxx.vote.db.DBVote
import model.TalkRating
import model.Vote
import org.ektorp.CouchDbConnector
import org.ektorp.UpdateHandlerRequest
import org.ektorp.ViewResult
import org.ektorp.support.CouchDbRepositorySupport
import org.ektorp.support.UpdateHandler
import org.ektorp.support.View

@UpdateHandler(name = "vote", file = "vote_update_handler.js")
@View(name = "top", map = "classpath:map_rating.js", reduce = "_stats")
class VoteRepository extends CouchDbRepositorySupport<DBVote> {
    final TalkService talkService

    @Inject
    VoteRepository(CouchDbConnector db, TalkService talkService) {
        super(DBVote, db, "vote")
        this.talkService = talkService
        initStandardDesignDocument()
    }

    void update(Vote vote, String conference) {
        update(new DBVote(vote, conference))
    }

    @Override
    void update(DBVote entity) {
        db.callUpdateHandler(new UpdateHandlerRequest()
                .body(entity)
                .designDocId("_design/vote")
                .functionName("vote")
                .docId("${entity.talkId}_${entity.user}"))
    }

    Iterable<TalkRating> stats(String conference) {
        def ratings = db.queryView(createQuery("top").group(true).keys(talkService.talkIdsForConference(conference))).rows
        ratings.collect(this.&ratingFromRow)
                .toSorted { rating ->
                    rating.avg
                }.reverse()
    }

    private static TalkRating ratingFromRow(ViewResult.Row row) {
        new TalkRating(row.key,
                row.valueAsNode.get("sum").asLong(),
                row.valueAsNode.get("count").asLong())
    }
}
