package devoxx.vote.db

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import model.Detail
import model.Vote
import org.ektorp.support.CouchDbDocument


@JsonIgnoreProperties(["additionalProperties", "rating"])
class DBVote extends CouchDbDocument {
    @Delegate
    @JsonIgnore
    final Vote vote

    @JsonProperty("conference")
    String conference

    @JsonProperty("oldRating")
    List<Long> oldRating

    @JsonProperty("timestamp")
    Long timestamp

    DBVote(Vote vote, String conference) {
        this.vote = vote
        this.conference = conference
        this.id = "${vote.talkId}_${vote.user}"
        this.timestamp = System.currentTimeMillis()
    }

    DBVote() {
        vote = new Vote()
    }

    @JsonProperty("details")
    public Set<Detail> getDetails() {
        if (vote.details == null || vote.details.empty) {
            [new Detail().withRating(getRating()).withAspect("default")].toSet()
        } else {
            vote.details
        }
    }
}
