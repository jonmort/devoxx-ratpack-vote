package devoxx.vote.endpoints

import model.Conference
import ratpack.func.Action
import ratpack.groovy.Groovy
import ratpack.handling.Chain

import static ratpack.jackson.Jackson.json

class CategoriesEndpoint implements Action<Chain> {
    @Override
    void execute(Chain chain) throws Exception {
        Groovy.chain(chain) {
            get { Conference conference ->
                response.headers.add('Access-Control-Allow-Origin', '*')
                render(json([
                        talkTypes: conference.slots*.talk*.talkType.unique(),
                        days     : conference.slots*.day.unique(),
                        tracks   : conference.slots*.talk*.track.unique()
                ]))
            }
        }
    }
}
