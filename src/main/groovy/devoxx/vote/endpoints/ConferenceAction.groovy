package devoxx.vote.endpoints

import com.google.inject.Inject
import devoxx.vote.TalkService
import groovy.util.logging.Slf4j
import model.Conference
import ratpack.func.Action
import ratpack.groovy.Groovy
import ratpack.handling.Chain

@Slf4j
class ConferenceAction implements Action<Chain> {
    TalkService talkService

    @Inject
    ConferenceAction(TalkService talkService) {
        this.talkService = talkService
    }

    @Override
    void execute(Chain chain) throws Exception {
        Groovy.chain(chain) {
            all {
                def conferenceId = pathTokens.get('conference')
                if (conferenceId) {

                    def conference = talkService.conferences[conferenceId]
                    if (conference) {
                        context.next(single(Conference, conference))
                        return
                    }
                }
                response.status(404)
                response.send("Conference Not Found")
            }
        }
    }
}
