package devoxx.vote.endpoints

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.Scopes
import com.google.inject.Singleton

import javax.validation.Validation
import javax.validation.Validator

class EndpointsModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(SubmitVoteEndpoint).in(Scopes.SINGLETON)
        bind(TopTalksEndpoint).in(Scopes.SINGLETON)
        bind(CategoriesEndpoint).in(Scopes.SINGLETON)
        bind(ConferenceAction).in(Scopes.SINGLETON)
    }

    @Provides
    @Singleton
    Validator provideValidator() {
        Validation.buildDefaultValidatorFactory().validator
    }
}
