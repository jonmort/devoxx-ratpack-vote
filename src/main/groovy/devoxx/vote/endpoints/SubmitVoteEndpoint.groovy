package devoxx.vote.endpoints

import com.google.inject.Inject
import devoxx.vote.TalkService
import devoxx.vote.VoteRepository
import groovy.util.logging.Slf4j
import model.Conference
import model.Vote
import model.VoteError
import ratpack.exec.Blocking
import ratpack.func.Action
import ratpack.groovy.Groovy
import ratpack.handling.Chain

import javax.validation.ConstraintViolation
import javax.validation.Validator

import static ratpack.jackson.Jackson.json

@Slf4j
class SubmitVoteEndpoint implements Action<Chain> {

    final Validator validator
    final TalkService talkService
    final VoteRepository voteRepository

    @Inject
    SubmitVoteEndpoint(Validator validator, TalkService talkService, VoteRepository voteRepository) {
        this.validator = validator
        this.talkService = talkService
        this.voteRepository = voteRepository
    }

    @Override
    void execute(Chain chain) throws Exception {
        Groovy.chain(chain) {
            post { Conference conference ->
                parse(Vote) then { Vote v ->
                    log.info(v.toString())
                    final Set<ConstraintViolation<Vote>> constraintViolations =
                            validator.validate(v)
                    if (constraintViolations.empty) {

                        if (v.rating == null && (v.details == null || v.details.empty)) {
                            response.status(400)
                            log.warn("Vote cast with no rating or details")
                            render(json(new VoteError().withMessage("rating or details must be provided")))
                        }

                        else if (!talkService.talkExists(v.talkId)) {
                            response.status(400)
                            log.warn("Attempted to vote on a talk that doesn't exist: {}", v)
                            render(json(new VoteError().withMessage("Talk ${v.talkId} does not exist")))
                        } else if (!talkService.talkOpen(v.talkId)) {
                            response.status(400)
                            log.info("Attempted to vote on talk that isn't open: {}", v)
                            render(json(new VoteError().withMessage("Cannot vote on talk yet")))
                        } else {
                            Blocking.get {
                                voteRepository.update(v, conference.name)
                                v
                            } then {
                                render(json(it))
                            }
                        }
                    } else {
                        response.status(400)
                        def e = errors(constraintViolations)
                        log.warn("Vote failed constraints {}, Vote: {}", e, v)
                        render(json(new VoteError().withMessage(e)))
                    }
                }
            }
        }
    }

    private static String errors(Set<ConstraintViolation<Vote>> constraintViolations) {
        constraintViolations.collect { ConstraintViolation cv ->
            "${cv.propertyPath} ${cv.message}"
        }.join(",")
    }
}
