package devoxx.vote.endpoints

import com.google.inject.Inject
import devoxx.vote.TalkService
import devoxx.vote.VoteRepository
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.util.logging.Slf4j
import model.Conference
import model.Talk
import model.TalkRating
import ratpack.exec.Blocking
import ratpack.func.Action
import ratpack.groovy.Groovy
import ratpack.handling.Chain

import static ratpack.jackson.Jackson.json

@Slf4j
class TopTalksEndpoint implements Action<Chain> {
    TalkService talkService
    VoteRepository voteRepository

    @Inject
    TopTalksEndpoint(TalkService talkService, VoteRepository voteRepository) {
        this.talkService = talkService
        this.voteRepository = voteRepository
    }

    @Override
    void execute(Chain chain) throws Exception {
        Groovy.chain(chain) {
            get { Conference conference ->
                response.headers.add('Access-Control-Allow-Origin', '*')
                def limit = Integer.parseInt(request.getQueryParams().get("limit") ?: "10")

                if (limit < 1 || limit > 100) {
                    response.status(400).send("limit must be a positive integer less than 100")
                } else {

                    def talkType = request.getQueryParams().get("talkType")
                    def day = request.getQueryParams().get("day")
                    def track = request.getQueryParams().get("track")
                    Blocking.get {
                        def stats = voteRepository.stats(conference.name).findAll {
                            it.count >= 5 &&
                                    it.avg >= 3 &&
                                    (talkType == null || talkService.getTalkById(it.talkId).talkType == talkType) &&
                                    (day == null || talkService.getSlotByTalkId(it.talkId).day == day) &&
                                    (track == null || talkService.getTalkById(it.talkId).track == track)
                        }.take(limit).collect(this.&includeTalkInfo)
                        stats
                    } then { stats ->
                        render(json([talks: stats]))
                    }
                }
            }
        }
    }

    TalkResponse includeTalkInfo(TalkRating r) {
        def talk = talkService.getTalkById(r.talkId)
        new TalkResponse(talkRating: r, talk: talk)
    }

    /*
    combine the talk rating and the talk together
     */
    @EqualsAndHashCode
    @ToString
    static class TalkResponse {
        @SuppressWarnings("GroovyUnusedDeclaration")
        @Delegate(excludes = "additionalProperties") TalkRating talkRating
        @Delegate(excludes = "additionalProperties") Talk talk
    }
}
