package model

import groovy.transform.Immutable

@Immutable
class Conference {
    List<Slot> slots
    List<Speaker> speakers
    String name
}
