package model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString
import groovy.transform.TupleConstructor

@EqualsAndHashCode
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@TupleConstructor
class TalkRating {
    final String talkId
    final long sum
    final long count

    @JsonCreator
    TalkRating(
            @JsonProperty("talkId") String talkId,
            @JsonProperty("sum") long sum,
            @JsonProperty("count") long count) {
        this.talkId = talkId
        this.sum = sum
        this.count = count
    }

    double getAvg() {
        sum/count
    }
}
