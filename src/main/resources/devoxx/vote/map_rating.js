function (doc) {
  if (doc.details) {
    var rating = doc.details.map(function(detail){
        return detail.rating;
    }).reduce(function(x, y) {
        return x + y;
    });
    emit(doc.talkId, rating/doc.details.length);
  }
}
