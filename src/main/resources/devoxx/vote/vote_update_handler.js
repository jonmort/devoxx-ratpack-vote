function (doc, req) {
  var up = JSON.parse(req.body);
  if (!doc) {
    doc = up;
    doc['_id'] = up.talkId + '_' + up.user;
    return [doc, 'created']
  } else if (doc.rating) {
    if (doc.oldRating) {
      doc.oldRating.push(doc.rating);
    } else {
      doc.oldRating = [doc.rating];
    }
  }
  doc.rating = up.rating;
  return [doc, 'updated'];
}
