import com.codahale.metrics.graphite.GraphiteUDP
import com.google.common.io.Resources
import devoxx.vote.VoteModule
import devoxx.vote.endpoints.*
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ratpack.config.ConfigData
import ratpack.dropwizard.metrics.DropwizardMetricsConfig
import ratpack.dropwizard.metrics.DropwizardMetricsModule
import ratpack.handling.RequestLogger

import static ratpack.groovy.Groovy.ratpack

final Logger logger = LoggerFactory.getLogger("devoxx-vote")

ratpack {
    bindings {
        ConfigData configData = ConfigData.of { c ->
            c.yaml(Resources.asByteSource(this.class.classLoader.getResource("app-config.yml")))
                    .env("VOTE_")
                    .sysProps("vote.")
        }
        moduleConfig VoteModule, configData.get("/database", VoteModule.DbConfig)

        def metricsConfig = configData.get("/metrics", DropwizardMetricsConfig)
                .graphite {
            it.enable()
                    .prefix("d163dbe2-28b5-484e-b66c-152b3dc4b2ed ")
                    .sender(new GraphiteUDP("9620d336.carbon.hostedgraphite.com", 2003))
        }
        moduleConfig DropwizardMetricsModule, metricsConfig
        module EndpointsModule
    }

    handlers {
        all RequestLogger.ncsa(logger)

        prefix(":conference") {
            insert ConferenceAction
            prefix 'vote', SubmitVoteEndpoint
            prefix "top/talks", TopTalksEndpoint
            prefix "categories", CategoriesEndpoint
        }
    }
}

