package devoxx.vote

import com.fasterxml.jackson.databind.ObjectMapper
import devoxx.vote.db.DBVote
import groovy.json.JsonSlurper
import model.Detail
import model.Vote
import model.VoteError
import org.ektorp.BulkDeleteDocument
import org.ektorp.CouchDbConnector
import org.ektorp.CouchDbInstance
import org.ektorp.http.HttpClient
import org.ektorp.http.StdHttpClient
import org.ektorp.impl.StdCouchDbInstance
import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import ratpack.test.ServerBackedApplicationUnderTest
import ratpack.test.http.TestHttpClient
import spock.lang.Specification

class TopTalksSmokeSpec extends Specification {

    ServerBackedApplicationUnderTest aut = new GroovyRatpackMainApplicationUnderTest()
    @Delegate
    TestHttpClient client = TestHttpClient.testHttpClient(aut)
    final ObjectMapper mapper = new ObjectMapper()
    TalkService talkService = new TalkService()
    private String couchDbUrl = "http://192.168.99.100:5984"
    private String couchDbDatabase = "foo"

    def "Top Talks with no talks"() {
        given:
        clearVotes()
        when:
        get("/DV15/top/talks")
        then:
        assert response.statusCode == 200

        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        with(topTalks) {
            assert talks.empty
        }
    }


    def "Top Talks with one talk"() {
        given:
        def talkId = "BOX-7491"
        clearVotes()
        //lots of votes
        30.times { i ->
            postVote(new Vote().withTalkId(talkId).withRating(5).withUser("foo${i}"))
        }
        when:
        get("/DV15/top/talks")
        then:
        assert response.statusCode == 200

        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        with(topTalks) {
            assert !talks.empty

            def talk = talks[0]
            assert talk.summary
            assert talk.title
            assert talk.talkType
            assert talk.speakers
            assert !talk.speakers.empty
        }
    }

    def "Top Talks requires at least 5 votes"() {
        given:
        def talkId = "BOX-7491"
        clearVotes()
        4.times { i ->
            postVote(new Vote().withTalkId(talkId).withRating(5).withUser("foo${i}"))
        }
        when:
        get("/DV15/top/talks")
        then:
        assert response.statusCode == 200

        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        with(topTalks) {
            assert talks.empty
        }
        when:
        postVote(new Vote().withTalkId(talkId).withRating(5).withUser("foo4"))
        get("/DV15/top/talks")
        topTalks = new JsonSlurper().parse(response.body.inputStream)
        then:
        assert response.statusCode == 200
        with(topTalks) {
            assert !talks.empty
        }
    }

    def "Top Talks requires an average vote of 3 or higher"() {
        given:
        def talkId = "BOX-7491"
        clearVotes()
        5.times { i ->
            postVote(new Vote().withTalkId(talkId).withRating(2).withUser("foo${i}"))
        }
        when:
        get("/DV15/top/talks")
        then:
        assert response.statusCode == 200

        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        with(topTalks) {
            assert talks.empty
        }
        when:
        3.times { i ->
            postVote(new Vote().withTalkId(talkId).withRating(5).withUser("foo${i + 5}"))
        }
        get("/DV15/top/talks")
        topTalks = new JsonSlurper().parse(response.body.inputStream)
        then:
        assert response.statusCode == 200
        assert topTalks.talks[0].avg > 3
        assert !topTalks.talks.empty
    }

    def "Top Talks has a limit of 100"() {
        given:
        twoHundredVotes()

        when:
        get("/DV15/top/talks?limit=101")
        then:
        assert response.statusCode == 400
        assert response.body.text == "limit must be a positive integer less than 100"
    }

    def "Top Talks returns a list of size limit"() {
        given:
        twoHundredVotes()

        when:
        get("/DV15/top/talks?limit=15")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        assert topTalks.talks.size() == 15
    }

    def "Top Talks returns 10 talks by default"() {
        given:
        twoHundredVotes()

        when:
        get("/DV15/top/talks")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        assert topTalks.talks.size() == 10
    }

    def "Top Talks orders the talks by average rating"() {
        given:
        twoHundredVotes()

        when:
        get("/DV15/top/talks?limit=100")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        assert topTalks.talks.size() == 100
        topTalks.talks.collate(2, 1, false).each {
            def i = it[0]
            def j = it[1]
            assert i.avg >= j.avg
        }
    }

    def "Top Talks can filter by category Tools-in-Action"() {
        given:
        twoHundredVotes()

        def talkType = "Tools-in-Action"
        when:
        get("/DV15/top/talks?limit=100&talkType=${talkType}")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        topTalks.talks.each { it ->
            def talk = talkService.getTalkById(it.talkId)
            assert talk.talkType == talkType
        }
    }

    def "Top Talks can filter by category BOF (Bird of a Feather)"() {
        given:
        twoHundredVotes()

        def talkType = "BOF (Bird of a Feather)"
        when:
        get("/DV15/top/talks?limit=100&talkType=${URLEncoder.encode(talkType, 'UTF-8')}")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        topTalks.talks.each { it ->
            def talk = talkService.getTalkById(it.talkId)
            assert talk.talkType == talkType
        }
    }

    def "Top Talks can filter by day monday"() {
        given:
        twoHundredVotes()

        def day = "monday"
        when:
        get("/DV15/top/talks?limit=100&day=${URLEncoder.encode(day, 'UTF-8')}")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        topTalks.talks.each { it ->
            def slot = talkService.getSlotByTalkId(it.talkId)
            assert slot.day == day
        }
    }

    def "Top Talks can filter by day tuesday"() {
        given:
        twoHundredVotes()

        def day = "tuesday"
        when:
        get("/DV15/top/talks?limit=100&day=${URLEncoder.encode(day, 'UTF-8')}")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        topTalks.talks.each { it ->
            def slot = talkService.getSlotByTalkId(it.talkId)
            assert slot.day == day
        }
    }

    def "Top Talks can filter by track JVM Languages"() {
        given:
        twoHundredVotes()

        def track = "JVM Languages"
        when:
        get("/DV15/top/talks?limit=100&track=${URLEncoder.encode(track, 'UTF-8')}")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        topTalks.talks.each { it ->
            def talk = talkService.getTalkById(it.talkId)
            assert talk.track == track
        }
    }

    def "Top Talks can filter by track Methodology & DevOps"() {
        given:
        twoHundredVotes()

        def track = "Methodology & DevOps"
        when:
        get("/DV15/top/talks?limit=100&track=${URLEncoder.encode(track, 'UTF-8')}")
        then:
        assert response.statusCode == 200
        def topTalks = new JsonSlurper().parse(response.body.inputStream)
        topTalks.talks.each { it ->
            def talk = talkService.getTalkById(it.talkId)
            assert talk.track == track
        }
    }

    def "List categories for conference"() {
        when:
        get("/DV15/categories")
        then:
        assert response.statusCode == 200
        def categories = new JsonSlurper().parse(response.body.inputStream)
        assert categories.days as Set == ['monday', 'tuesday', 'wednesday', 'thursday', 'friday'] as Set
        assert categories.talkTypes as Set == ['Tools-in-Action', 'Quickie', 'University', 'Conference',
                                                  'Ignite Sessions', "Hand's on Labs", 'BOF (Bird of a Feather)',
                                                  'Keynote', 'Startup presentation'] as Set
        assert categories.tracks as Set == ['Web & HTML5', 'Server Side Java', 'Future<Devoxx>', 'Java SE',
                                            'JVM Languages', 'Startups', 'Architecture & Security',
                                            'Methodology & DevOps', 'Cloud & BigData', 'Mobile'] as Set
    }

    def cleanup() {
        aut.stop()
    }

    def twoHundredVotes() {
        clearVotes()
        def talks = talkService.talkIdsForConference("DV15").take(200)
        def r = new Random()
        def aspects = ['foo', 'bar', 'baz', 'qux']
        def votes = talks.collect { talk ->
            (0..5).collect { i ->
                new DBVote(new Vote().withTalkId(talk)
                        .withDetails([new Detail()
                                              .withAspect(aspects.get(r.nextInt(aspects.size())))
                                              .withRating(r.nextInt(3) + 3).withReview("i can haz cheezburger")] as Set<Detail>)
                        .withUser("foo${i}"), "DV15")
            }
        }.flatten()
        bulkVote(votes)
    }

    def postVote(def vote) {
        requestSpec {
            it.body {
                it.type("application/json")
                it.text(mapper.writeValueAsString(vote))
            }
        }
        post("/DV15/vote")
    }

    def clearVotes() {
        CouchDbConnector db = getCouchDb()
        def voteRepository = new VoteRepository(db, talkService)

        def all = voteRepository.getAll()
        if (all) {
            db.executeBulk(all.collect { BulkDeleteDocument.of(it) })
            db.flushBulkBuffer()
            db.ensureFullCommit()
        }

    }

    def bulkVote(Collection<DBVote> votes) {
        CouchDbConnector db = getCouchDb()
        db.executeBulk(votes)
        db.flushBulkBuffer()
        db.ensureFullCommit()
    }

    private CouchDbConnector getCouchDb() {
        HttpClient httpClient = new StdHttpClient.Builder()
                .url(couchDbUrl)
                .build()
        CouchDbInstance dbInstance = new StdCouchDbInstance(httpClient)
        CouchDbConnector db = dbInstance.createConnector(couchDbDatabase, true)
        db
    }
}
