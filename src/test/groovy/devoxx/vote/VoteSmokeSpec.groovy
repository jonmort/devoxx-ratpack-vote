package devoxx.vote

import com.fasterxml.jackson.databind.ObjectMapper
import devoxx.vote.db.DBVote
import groovy.json.JsonSlurper
import model.Detail
import model.Vote
import model.VoteError
import org.apache.commons.lang.RandomStringUtils
import org.ektorp.BulkDeleteDocument
import org.ektorp.CouchDbConnector
import org.ektorp.CouchDbInstance
import org.ektorp.http.HttpClient
import org.ektorp.http.StdHttpClient
import org.ektorp.impl.StdCouchDbInstance
import ratpack.groovy.test.GroovyRatpackMainApplicationUnderTest
import ratpack.test.ServerBackedApplicationUnderTest
import ratpack.test.http.TestHttpClient
import spock.lang.Specification

class VoteSmokeSpec extends Specification {

    ServerBackedApplicationUnderTest aut = new GroovyRatpackMainApplicationUnderTest()
    @Delegate
    TestHttpClient client = TestHttpClient.testHttpClient(aut)
    final ObjectMapper mapper = new ObjectMapper()

    def "Valid vote should respond with vote (rating)"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withRating(5).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 200

            def resp = mapper.readValue(body.text, Vote)
            resp.user == "foo"
            resp.rating == 5L
            resp.talkId == talkId
        }

    }

    def "Valid vote should respond with vote (details) - no review"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(5).withAspect("foo")] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 200

            def resp = mapper.readValue(body.text, Vote)
            resp.user == "foo"
            resp.rating == null
            resp.talkId == talkId
            resp.details[0].rating == 5
            resp.details[0].aspect == "foo"
        }

    }

    def "Valid vote should respond with vote (details) - review"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(5).withAspect("foo").withReview("rather good")] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 200

            def resp = mapper.readValue(body.text, Vote)
            resp.user == "foo"
            resp.rating == null
            resp.talkId == talkId
            resp.details[0].rating == 5
            resp.details[0].aspect == "foo"
            resp.details[0].review == "rather good"
        }

    }

    def "Review too long"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(5).withAspect("foo").withReview(RandomStringUtils.randomAscii(1025))] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 400

            def resp = mapper.readValue(body.text, VoteError)
            resp.message == "details[].review size must be between 1 and 1024"
        }
    }

    def "Aspect required"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(5)] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 400

            def resp = mapper.readValue(body.text, VoteError)
            resp.message == "details[].aspect may not be null"
        }
    }

    def "Aspect length required"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(5).withAspect("")] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 400

            def resp = mapper.readValue(body.text, VoteError)
            resp.message == "details[].aspect size must be between 1 and 256"
        }
    }

    def "Rating required (details)"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withAspect("foo")] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 400

            def resp = mapper.readValue(body.text, VoteError)
            resp.message == "details[].rating may not be null"
        }
    }

    def "Review too short"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(5).withAspect("foo").withReview("")] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        with(response) {
            statusCode == 400

            def resp = mapper.readValue(body.text, VoteError)
            resp.message == "details[].review size must be between 1 and 1024"
        }
    }

    def "Negative rating"() {
        given:
        def talkid = "BOX-7491"
        when:
        def vote = new Vote().withRating(-1).withTalkId(talkid).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "rating must be greater than or equal to 1"
    }

    def "Zero  rating"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withRating(0).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "rating must be greater than or equal to 1"
    }

    def "Over 5 rating"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withRating(6).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "rating must be less than or equal to 5"
    }

    def "Negative rating (details)"() {
        given:
        def talkid = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(-1).withAspect("foo")] as Set).withTalkId(talkid).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "details[].rating must be greater than or equal to 1"
    }

    def "Zero rating (details)"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(0).withAspect("foo")] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "details[].rating must be greater than or equal to 1"
    }

    def "Over 5 rating (details)"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withDetails([new Detail().withRating(6).withAspect("foo")] as Set).withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "details[].rating must be less than or equal to 5"
    }

    def "Required rating or detail"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withTalkId(talkId).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "rating or details must be provided"
    }

    def "Required talkid"() {
        when:
        def vote = new Vote().withRating(5).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "talkId may not be null"
    }

    def "Non-existant talkid"() {
        given:
        def talkId = "Bar"
        when:
        def vote = new Vote().withTalkId(talkId).withRating(5).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "Talk Bar does not exist"
    }

    def "Closed Talk talkid"() {
        given:
        def talkId = "NOT-OPEN"
        when:
        def vote = new Vote().withTalkId(talkId).withRating(5).withUser("foo")
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "Cannot vote on talk yet"
    }

    def "Required user"() {
        given:
        def talkId = "BOX-7491"
        when:
        def vote = new Vote().withRating(5).withTalkId(talkId)
        postVote(vote)

        then:
        assert response.statusCode == 400

        def resp = mapper.readValue(response.body.text, VoteError)
        assert resp.message == "user may not be null"
    }

    def cleanup() {
        aut.stop()
    }

    def postVote(def vote) {
        requestSpec {
            it.body {
                it.type("application/json")
                it.text(mapper.writeValueAsString(vote))
            }
        }
        post("/DV15/vote")
    }
}
